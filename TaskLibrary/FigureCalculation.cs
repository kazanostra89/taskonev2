﻿using System;
using System.Collections.Generic;

namespace TaskLibrary
{
    public abstract class FigureCalculation
    {
        public static double CircleArea(double radius)
        {
            return Math.PI * Math.Pow(radius < 0 ? -radius : radius, 2.0);
        }

        public static double TriangleArea(double legOne, double legTwo, double legThree)
        {
            legOne = legOne < 0 ? -legOne : legOne;
            legTwo = legTwo < 0 ? -legTwo : legTwo;
            legThree = legThree < 0 ? -legThree : legThree;

            double semiPerimeter = (legOne + legTwo + legThree) / 2.0;

            return Math.Sqrt(semiPerimeter * (semiPerimeter - legOne)
                * (semiPerimeter - legTwo) * (semiPerimeter - legThree));
        }

        public static bool IsRightTriangle(double legOne, double legTwo, double legThree)
        {
            legOne = legOne < 0 ? -legOne : legOne;
            legTwo = legTwo < 0 ? -legTwo : legTwo;
            legThree = legThree < 0 ? -legThree : legThree;

            return Math.Pow(legOne, 2.0) + Math.Pow(legTwo, 2.0) == Math.Pow(legThree, 2.0)
                || Math.Pow(legTwo, 2.0) + Math.Pow(legThree, 2.0) == Math.Pow(legOne, 2.0)
                || Math.Pow(legThree, 2.0) + Math.Pow(legOne, 2.0) == Math.Pow(legTwo, 2.0);
        }

        public abstract double FigureArea(Dictionary<string, object> parameters);
    }
}
