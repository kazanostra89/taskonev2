﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskLibrary;

namespace TaskLibrary.Tests
{
    [TestClass]
    public class TaskLibraryTests
    {
        [TestMethod]
        public void IsRightTriangle_4and5and3_trueReturned()
        {
            //Arange
            double legOne = 4.0;
            double legTwo = 5.0;
            double legThree = 3.0;
            bool expected = true;

            //Act
            bool actual = FigureCalculation.IsRightTriangle(legOne, legTwo, legThree);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CircleArea_4_50dot3returned()
        {
            //Arange
            double radius = 4.0;
            double expected = 50.26548245743669;

            //Act
            double actual = FigureCalculation.CircleArea(radius);

            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
